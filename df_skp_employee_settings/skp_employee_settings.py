from openerp.osv import fields, osv
from datetime import datetime, date
import time
from mx import DateTime

class notification_skp_employee_process(osv.osv_memory):
    _name = "notification.skp.employee.process"
    _columns = {
        'name': fields.char('Notif', size=128),
    }
notification_skp_employee_process();
class skp_employee_settings(osv.osv_memory):
    _name = 'skp.employee.settings'
    _description = 'Konfigurasi Aplikasi SKP'

    _columns = {
            'name' : fields.char('Konfigurasi Integrasi SKP SIPKD', size=64, readonly=True),
            'param_type'     : fields.selection([('all', 'Semua'), ('skp', 'SKP'),
                                                      ('perilaku', 'Perilaku'), ('tk', 'Tambahan Dan Kreatifitas')], 'Jenis Kegiatan'
                                                     ),
            'param_list_of_ids' : fields.char('Long Char', size=400, ),
             'param_employee_id': fields.many2one('res.partner', 'Pegawai',),

            'param_target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     ),
            'param_all_company'     : fields.boolean('Semua OPD',),
            'param_target_period_year'     : fields.char('Periode Tahun', size=4, ),
        'param_custom_integer'     : fields.integer('Param Custom', size=2, ),
            'param_company_id' : fields.many2one('res.company','OPD')
    }
    _defaults = {
        'param_custom_integer' :-1,
    }

    def auto_recalculate_by_month_scheduler(self, cr, uid,param_year,param_company_id,delta_days, context=None):
        print "param_year  ",param_year
        print "param_company_id  ",param_company_id
        result =  self.action_recalculate_monthly_param(cr,uid,param_year,param_company_id,delta_days);
        print "monthly calculation done...."

        return result
    def auto_recalculate_by_year_scheduler(self, cr, uid,param_year,param_company_id, context=None):
        print "param_year  ",param_year
        print "param_company_id  ",param_company_id
        result =  self.action_recalculate_yearly_param(cr,uid,param_year,param_company_id);
        print "yearly calculation done...."

        return result

    def action_recalculate_monthly_param(self, cr, uid, ids, context=None):

        #
        #
        partner_pool = self.pool.get('res.partner')
        skp_employee_pool = self.pool.get('skp.employee')
        for param_obj in self.browse(cr, uid, ids, context=context):
                try :
                    param_year = param_obj.param_target_period_year
                    param_month = param_obj.param_target_period_month
                    param_company_id  = param_obj.param_company_id.id
                    is_single_calc=False



                    if not param_obj.param_employee_id :
                        #print "all emp"
                        employee_ids=partner_pool.search(cr, uid, [('company_id','=',param_company_id),('employee','=',True)
                                                     ], context=None)
                        employee_objs = partner_pool.read(cr, uid, employee_ids, ['id','user_id','company_id'], context=context)
                    else :
                        is_single_calc=True;
                        employee_objs = [param_obj.param_employee_id,]
                        #print "1 emp"

                    for employee_obj in employee_objs:

                        try :
                            if not is_single_calc:
                                employee_id=employee_obj['id']
                                is_can_calculate=False
                                if employee_obj['user_id']:
                                    employee_user_id=employee_obj['user_id'][0]
                                    is_can_calculate=True
                                if employee_obj['company_id']:
                                    employee_company_id=employee_obj['company_id'][0]
                            else :
                                employee_id=employee_obj.id
                                is_can_calculate=False
                                if employee_obj.user_id:
                                    employee_user_id=employee_obj.user_id.id
                                    is_can_calculate=True
                                if employee_obj.company_id:
                                    employee_company_id=employee_obj.company_id.id

                            now=DateTime.today();


                            curr_date =  DateTime.strptime(now.strftime('%Y-%m-%d'),'%Y-%m-%d')
                            minus_1_date = curr_date + DateTime.RelativeDateTime(days=delta_days)
                            now_day=curr_date.strftime('%Y-%m-%d')
                            day_minus_1=minus_1_date.strftime('%Y-%m-%d')
                            domain = param_year,employee_user_id,day_minus_1,now_day,param_year,employee_user_id,day_minus_1,now_day,param_year,employee_user_id,day_minus_1,now_day

                            list_period_month = self.get_period_months(cr,uid,domain)

                            if list_period_month:
                                for p_month in list_period_month :
                                    single_month = p_month and p_month[0] or '00'
                                    skp_employee_ids = skp_employee_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                                       ('target_period_year', '=', param_year),
                                                                    ('target_period_month', '=', single_month),

                                                                       ], context=None)

                                    for skp_employee_obj in  skp_employee_pool.browse(cr, uid, skp_employee_ids, context=context):
                                        try :

                                                skp_state_count,jml_skp,jml_all_skp,nilai_skp,nilai_skp_percent,fn_nilai_tambahan,fn_nilai_kreatifitas,nilai_skp_tambahan_percent,jml_perilaku,nilai_perilaku,nilai_perilaku_percent,nilai_pelayanan,nilai_disiplin,nilai_komitmen,nilai_integritas,nilai_kerjasama,nilai_kepemimpinan,nilai_total,nilai_tpp =skp_employee_pool._get_detail_nilai_skp_employee(cr, uid, skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year, context=None)


                                                update_values = {
                                                    'skp_state_count': skp_state_count,
                                                    'jml_skp': jml_skp,
                                                    'jml_all_skp': jml_all_skp,
                                                    'nilai_skp': nilai_skp,
                                                    'fn_nilai_tambahan': fn_nilai_tambahan,
                                                    'fn_nilai_kreatifitas': fn_nilai_kreatifitas,
                                                    'nilai_skp_percent': nilai_skp_percent,
                                                    'nilai_skp_tambahan_percent': nilai_skp_tambahan_percent,

                                                    'jml_perilaku': jml_perilaku,
                                                    'nilai_perilaku': nilai_perilaku,
                                                    'nilai_perilaku_percent': nilai_perilaku_percent,
                                                    'nilai_pelayanan': nilai_pelayanan,
                                                     'nilai_integritas': nilai_integritas,
                                                     'nilai_komitmen':nilai_komitmen,
                                                     'nilai_disiplin': nilai_disiplin,
                                                     'nilai_kerjasama': nilai_kerjasama,
                                                     'nilai_kepemimpinan':nilai_kepemimpinan,

                                                    'nilai_total': nilai_total,
                                                    'nilai_tpp': nilai_tpp,
                                                }
                                                skp_employee_pool.write(cr , uid,[skp_employee_obj.id,], update_values, context=None)
                                        except:
                                            print "Some process cannot be calculate on calculate monthly";


                        except:
                            print "Some process cannot be calculate on load person";
                except:
                    print "Some process cannot be calculate on loop param";


                    return True



skp_employee_settings()