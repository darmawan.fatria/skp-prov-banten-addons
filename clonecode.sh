#! /bin/sh

git config --global "darmawan.fatria"

git config --global user.email "darmawan.fatria@gmail.com"

echo -e "\n==== Start Clone code ===="
echo -e "\n---- 1. main odoo 8 ----"
git clone https://github.com/darfat/odoo-server8.git
echo -e "\n---- 2. anjab ----"
git clone https://gitlab.com/darmawan.fatria/provjabar-anjab-addons.git
echo -e "\n---- 3. skp guru ----"
git clone https://gitlab.com/darmawan.fatria/provjabar-skp-guru-addons.git
echo -e "\n---- 4. sipkd ----"
git clone https://gitlab.com/darmawan.fatria/provjabar-skp-sipkd-addons.git
echo -e "\n---- 5. siao ----"
git clone https://gitlab.com/darmawan.fatria/provjabar-skp-siao-addons.git
echo -e "\n---- 6. skp notification ----"
git clone https://gitlab.com/darmawan.fatria/provjabar-skp-notification-addons.git
echo -e "\n---- 7. puslia ----"
git clone https://gitlab.com/darmawan.fatria/provjabar-skp-puslia-addons.git
echo -e "\n---- 8. multi jabatan ----"
git clone https://gitlab.com/darmawan.fatria/provjabar-skp-dashboard-addons.git
echo -e "\n---- 9. complaint ----"
git clone https://gitlab.com/darmawan.fatria/provjabar-skp-complaint-addons.git
echo -e "\n---- 10. jafung ----"
git clone https://gitlab.com/darmawan.fatria/provjabar-jafung-addons.git
echo -e "\n---- 11. pelayanan pendidikan ----"
git clone https://gitlab.com/darmawan.fatria/provjabar-pelayanan-pendidikan-addons.git
echo -e "\n---- 12. struktural ----"
git clone https://gitlab.com/darmawan.fatria/provjabar-struktural-addons.git
echo -e "\n---- 13. skp core ----"
git clone https://gitlab.com/darmawan.fatria/provjabar-skp-addons.git

echo -e "\n==== Finished ...===="
