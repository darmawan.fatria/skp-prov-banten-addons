from openerp.osv import fields, osv
import time

class skp_target_recap_report(osv.osv_memory):
    
    _name = "skp.target.recap.report"
    _columns = {
        'company_id'        : fields.many2one('res.company', 'OPD'),
        'biro_id'        : fields.many2one('partner.employee.biro', 'Biro'),
        'period_year'       : fields.char('Periode Tahun', size=4, required=True),
        'is_kepala_opd'       : fields.boolean('Hanya Kepala OPD'),
    }
    _defaults = {
        'period_year':lambda *args: time.strftime('%Y'),
        'is_kepala_opd':False,
        
    }
    
    def get_skp_target_recap_report(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'skp.target.recap.report',
            'form': value
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'skp.target.recap.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
skp_target_recap_report()
