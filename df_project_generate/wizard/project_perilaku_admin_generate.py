from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime,timedelta
import time
from mx import DateTime

class project_perilaku_admin_generate(osv.Model):
    _name = "project.perilaku.admin.generate"
    _description = "Generate Perilaku By Admin Bulanan"
    
    _columns = {
        'name'     : fields.char('Nama Kegiatan', size=128, required=True),
        
        'lama_kegiatan'     : fields.integer('Lama Kegiatan',readonly=True),
        'company_id': fields.many2one('res.company', 'OPD', ),
        'satuan_lama_kegiatan'     : fields.selection([('bulan', 'Bulan')],'Satuan Waktu Lama Kegiatan',readonly=True),
        'date_start'     : fields.date('Periode Awal Generate'),
        'target_period_year'     : fields.char('Periode Tahun', size=4, required=True),
        }
    _defaults = {
        'name' : "Realisasi Perilaku Kerja Bulan  ",
        'lama_kegiatan' : 12,
        'satuan_lama_kegiatan':'bulan',
        'target_period_year':lambda *args: time.strftime('%Y'),
       
        }
    def generate_task_perilaku_admin_realisasi_per_tahun(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task =  {}
        task_pool = self.pool.get('project.perilaku')
        user_pool = self.pool.get('res.users')
        partner_pool = self.pool.get('res.partner')
        company_pool = self.pool.get('res.company')
        for task_generate in self.browse(cr, uid, ids, context=context):
            company_ids = []
            if task_generate.company_id :
                company_ids.append(task_generate.company_id.id)
            else :
                company_ids = company_pool.search(cr,uid,[('employee_id_kepala_instansi','!=',False)],context=None);
            for company_id in company_ids :
                user_ids = user_pool.search(cr, uid, [('company_id','=',company_id)], context=None)
                
                for user_obj in  user_pool.browse(cr,uid,user_ids,context=None):
                    #check Duplicate
                    #Init Field
                    
                    user_id = user_obj.id
                    description=''
                    lama_kegiatan=task_generate.lama_kegiatan
                    
                    target_period_year = task_generate.target_period_year
                    target_period_month='xx'
                    date_start='xx'
                    date_end='xx'
                    company_id=None
                    currency_id=None
                    user_id_bkd=None
                    employee = user_obj.partner_id
                    
                    if not employee :
                        continue;
                    else :
                        company = employee.company_id
                        company_id = company.id
                        currency_id= employee.company_id.currency_id.id
                        if not employee.employee:
                            continue;
                         
                        if not company_id :
                            continue;
                        
                    user_id_bkd = company.user_id_bkd.id
                    user_id_atasan = employee.user_id_atasan.user_id.id
                    user_id_banding = employee.user_id_banding.user_id.id
                    employee_job_type = employee.job_type
                    task.update({
                                       'user_id':user_id,
                                       'company_id':company_id,
                                       'name': task_generate.name,
                                       'target_period_year': target_period_year,
                                        'user_id_atasan': user_id_atasan or False,
                                        'user_id_banding':user_id_banding or False,
                                        'user_id_bkd':user_id_bkd or False,
                                        'currency_id':currency_id,
                                        'employee_job_type':employee_job_type,
                                        'active':True,
                                       })
                    #Update Task Target Bulanan
                    now=DateTime.today();
                    first_task_id=None
                        
                    if task_generate.date_start :
                            curr_date = DateTime.strptime(task_generate.date_start,'%Y-%m-%d')
                    else :
                            january=DateTime.Date(now.year,1,1)
                            curr_date =  DateTime.strptime(january.strftime('%Y-%m-%d'),'%Y-%m-%d')
                    
                    first_date =curr_date
                    for i in range(0,lama_kegiatan):
                            next_date = curr_date + DateTime.RelativeDateTime(months=i)
                            target_period_month=next_date.strftime('%m')
                            task.update({
                                       'target_period_month':target_period_month,
                                       'name': '%s %s' % (task_generate.name,target_period_month),
                                       
                             })
                            #Check Duplicate Do Not Create
                            task_ids = task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_month','=',target_period_month),('target_period_year','=',target_period_year),
                                                            ('state','!=','draft')], context=None)
                            if task_ids:
                                continue;
                            else : 
                                #Delete Duplicate
                                task_ids = task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_month','=',target_period_month),('target_period_year','=',target_period_year),
                                                                      ('state','=','draft')], context=None)
                                task_pool.action_cancel_realisasi(cr, user_id, task_ids, context=None)
                        
                            date_start=first_date
                            date_end=first_date
                            state='draft';
                            task.update({
                                            'state':state,
                                    })
                            #insert task
                            task_id = task_pool.create(cr, user_id, task,context)
                
            
            
        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.message.perilaku',
                'target': 'new',
                'context': {
                    'default_message_info': 'Tugas Perilaku Bulanan Berhasil Digenerate',
                    }
            }
        
   
project_perilaku_admin_generate()
class notification_message_task_perilaku_admin(osv.osv_memory):
    _name = 'notification.message.perilaku.admin'
    _description = 'Pesan Popup Notifikasi'
    _columns = {
        'message_info': fields.text('Pesan'),
        }
notification_message_task_perilaku_admin()    