from openerp.osv import osv
from openerp.tools.translate import _
from openerp import pooler
from openerp import SUPERUSER_ID

class project_yearly_massive_process(osv.osv):
    _name = "project.yearly.massive.process"
    _description = "Proses Select All SKP Tahunan"
    def project_yearly_done_all(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        project_yearly_pool = pool_obj.get('project.project')

        if not ids :
            return {'type': 'ir.actions.act_window_close'}
        process_ids = []
        for obj in project_yearly_pool.read(cr,uid,context['active_ids'],['id','work_state','target_realiasi_notsame','biaya_verifikasi_notsame'], context) :
            if obj['work_state'] == 'evaluated' :
                if not obj['target_realiasi_notsame'] and not obj['biaya_verifikasi_notsame']  :
                    process_ids.append(obj['id'])
               
            
        for proces_is in process_ids : 
            project_yearly_pool.action_done(cr,uid,[proces_is],context);
        
        return {'type': 'ir.actions.act_window_close'}
    
project_yearly_massive_process()
class project_yearly_recalculate_massive_process(osv.osv):
    _name = "project.yearly.recalculate.massive.process"
    _description = "Proses Select All Calculate Tahunan"
    def project_yearly_recalculate_all(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        project_yearly_pool = pool_obj.get('project.project')

        if not ids :
            return {'type': 'ir.actions.act_window_close'}
        process_ids = []
        for obj in project_yearly_pool.read(cr,uid,context['active_ids'],['id','work_state','target_realiasi_notsame','biaya_verifikasi_notsame'], context) :
            if obj['work_state'] == 'done' :
                    process_ids.append(obj['id'])
               
        
        for proces_is in process_ids : 
            update_poin = {             'nilai_akhir': 0,
                                        'indeks_nilai': False,
                                        'jumlah_perhitungan':0,
                                        #'use_target_for_calculation': False,
                                         }
            project_yearly_pool.write(cr, SUPERUSER_ID, [proces_is], update_poin, context=context)
            project_yearly_pool.set_evaluated_yearly(cr, SUPERUSER_ID,[proces_is], context=context)
        
        return {'type': 'ir.actions.act_window_close'}
    
project_yearly_recalculate_massive_process()

class project_yearly_recalculate_admin_massive_process(osv.osv):
    _name = "project.yearly.recalculate.admin.massive"
    _description = "Proses Select All Calculate Tahunan"
    def project_yearly_recalculate_all(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        project_yearly_pool = pool_obj.get('project.project')

        if not ids :
            return {'type': 'ir.actions.act_window_close'}
        process_ids = []
        for obj in project_yearly_pool.read(cr,uid,context['active_ids'],['id','work_state','target_realiasi_notsame','biaya_verifikasi_notsame'], context) :
            if obj['work_state'] == 'done' :
                    process_ids.append(obj['id'])
        
        for proces_is in process_ids : 
            update_poin = {             'nilai_akhir': 0,
                                        'indeks_nilai': False,
                                        'jumlah_perhitungan':0,
                                        #'use_target_for_calculation': False,
                                         }
            project_yearly_pool.write(cr, SUPERUSER_ID, [proces_is,], update_poin, context=context)
            project_yearly_pool.set_evaluated_yearly(cr, SUPERUSER_ID,[proces_is,], context=context)
            project_yearly_pool.do_task_poin_calculation(cr, SUPERUSER_ID,[proces_is,], context=context)
            project_yearly_pool.set_done(cr, SUPERUSER_ID,[proces_is,], context=context)
        
        return {'type': 'ir.actions.act_window_close'}
    
project_yearly_recalculate_admin_massive_process()
class project_yearly_evaluated_massive_process(osv.osv):
    _name = "project.yearly.evaluated.massive"
    _description = "Proses Select All SKP Tahunan | Set Evaluated"
    
    def project_yearly_evaluated_all(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        project_yearly_pool = pool_obj.get('project.project')

        if not ids :
            return {'type': 'ir.actions.act_window_close'}
        process_ids = []
        for obj in project_yearly_pool.read(cr,uid,context['active_ids'],['id','work_state'], context) :
            if obj['work_state'] == 'propose' :
                process_ids.append(obj['id'])
               
            
        for proces_is in process_ids :
            project_yearly_pool.set_evaluated_yearly(cr,SUPERUSER_ID,[proces_is,],context);
        
        return {'type': 'ir.actions.act_window_close'}
    
project_yearly_evaluated_massive_process()
class project_tambahan_yearly_massive_process(osv.osv):
    _name = "project.tambahan.yearly.massive.process"
    _description = "Proses Select All Tugas Tambahan Tahunan"
    def project_tambahan_yearly_done_all(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        project_yearly_pool = pool_obj.get('project.tambahan.yearly')

        if not ids :
            return {'type': 'ir.actions.act_window_close'}
        process_ids = []
        for obj in project_yearly_pool.read(cr,uid,context['active_ids'],['id','state'], context) :
            if obj['state']=='evaluated' :
                process_ids.append(obj['id'])
               
            
        for proces_is in process_ids :
            project_yearly_pool.action_done(cr,uid,[proces_is,],context);
        
        return {'type': 'ir.actions.act_window_close'}
    
   

project_tambahan_yearly_massive_process()
class project_tambahan_yearly_evaluated_massive_process(osv.osv):
    _name = "project.tambahan.yearly.evaluated.massive"
    _description = "Proses Select All Tugas Tambahan Tahunan | Evaluated"
    
    def project_tambahan_yearly_evaluated_all(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        project_yearly_pool = pool_obj.get('project.tambahan.yearly')
        if not ids :
            return {'type': 'ir.actions.act_window_close'}
        process_ids = []
        for obj in project_yearly_pool.read(cr,uid,context['active_ids'],['id','state'], context) :
            if obj['state']=='propose' :
                process_ids.append(obj['id'])
        for proces_is in process_ids :
            project_yearly_pool.set_evaluated(cr,SUPERUSER_ID,[proces_is],context);
        
        return {'type': 'ir.actions.act_window_close'}
   

project_tambahan_yearly_evaluated_massive_process()

'''
class project_tambahan_yearly_temp_calc_massive_process(osv.osv):
    _name = "project.tambahan.yearly.temp.calc.massive"
    _description = "Proses Select All Tugas Tambahan Tahunan|Nilai Sementara"
   
    
    def project_tambahan_yearly_calculate_temp_poin_all(self, cr, uid, ids, context=None):

        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        project_yearly_pool = pool_obj.get('project.tambahan.yearly')
        if not ids :
            return {'type': 'ir.actions.act_window_close'}
        process_ids = []
        for obj in project_yearly_pool.read(cr,uid,context['active_ids'],['id','state'], context) :
            if obj['state']=='evaluated' :
                process_ids.append(obj['id'])
        for proces_is in process_ids :
            project_yearly_pool.do_task_poin_calculation_temporary(cr,SUPERUSER_ID,[proces_is],context);
        return {'type': 'ir.actions.act_window_close'}

project_tambahan_yearly_temp_calc_massive_process()
'''