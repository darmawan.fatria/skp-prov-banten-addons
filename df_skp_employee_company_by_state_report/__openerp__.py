##############################################################################
#
#    Darmawan Fatriananda
#    BKD Jabar Prov
#    http://mediasee.net
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Laporan Realisasi Kegiatan Per OPD",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Penilaian Prestasi Kerja / Laporan",
    "description": "Laporan Realisasi Kegiatan Per OPD ",
    "website" : "www.mediasee.net",
    "license" : "GPL-3",
    "depends": ['df_skp_employee'],
    'data': ["skp_recap_by_state_report.xml",],
    'installable': True,
    'active': False,
}
