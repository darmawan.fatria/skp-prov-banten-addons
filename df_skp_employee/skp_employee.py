# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime



class skp_employee(osv.Model):
    _name = 'skp.employee'
    _description = 'Rekapitulasi Bulanan Pegawai'
    
    def _get_nilai_skp_percent(self, cr, uid, ids, field_names, args, context=None):
        res = {}
       
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            nilai_skp = self._get_nilai_skp(cr, uid, [skp_employee_obj.id], field_names, args, context)
            if nilai_skp and nilai_skp.get(skp_employee_obj.id)>0.0:
                res[skp_employee_obj.id] =  nilai_skp.get(skp_employee_obj.id) * 0.6
            else : 
                res[skp_employee_obj.id] =0.0
        return res
   
    def _get_nilai_skp_tambahan_percent(self, cr, uid, ids, field_names, args, context=None):
        res = {}
       
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            nilai_skp = self._get_nilai_skp(cr, uid, [skp_employee_obj.id], field_names, args, context)
            nilai_tambahan = self._get_nilai_task_tambahan(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            nilai_kreatifitas = self._get_nilai_task_kreatifitas(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            
            if nilai_skp:
                res[skp_employee_obj.id] = ( nilai_skp.get(skp_employee_obj.id) * 0.6 )  + (nilai_tambahan or 0) + (nilai_kreatifitas or 0)
            else : 
                res[skp_employee_obj.id] =0.0
        return res
    
    def _get_nilai_perilaku_percent(self, cr, uid, ids, field_names, args, context=None):
        res = {}
       
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            nilai_perilaku = self._get_nilai_perilaku(cr, uid, [skp_employee_obj.id], field_names, args, context)
            if nilai_perilaku and nilai_perilaku.get(skp_employee_obj.id)>0.0:
                res[skp_employee_obj.id] =  nilai_perilaku.get(skp_employee_obj.id) * 0.4   
            else : 
                res[skp_employee_obj.id] =0.0
        return res
   
    def _get_nilai_total(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        "primt : _get_nilai_skp_tambahan_percent ========================= "
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            nilai_total=0.0
            nilai_tambahan=0
            nilai_skp = self._get_nilai_skp_tambahan_percent(cr, uid, [skp_employee_obj.id], field_names, args, context)
            nilai_perilaku = self._get_nilai_perilaku_percent(cr, uid, [skp_employee_obj.id], field_names, args, context)
            
            if nilai_skp and nilai_skp.get(skp_employee_obj.id)>0.0:
                nilai_total +=  nilai_skp.get(skp_employee_obj.id) 
            if nilai_perilaku and nilai_perilaku.get(skp_employee_obj.id)>0.0:
                nilai_total +=  nilai_perilaku.get(skp_employee_obj.id) 
            
            
            res[skp_employee_obj.id] =nilai_total
        return res
    def _get_nilai_total_sementara(self, cr, uid, ids, field_names, args, context=None):
        res = {}
       
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            nilai_total=0.0
            nilai_tambahan=0
            nilai_skp = self._get_nilai_skp_sementara_percent(cr, uid, [skp_employee_obj.id], field_names, args, context)
            nilai_perilaku = self._get_nilai_perilaku_percent(cr, uid, [skp_employee_obj.id], field_names, args, context)
            nilai_tambahan = self._get_nilai_task_tambahan(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            nilai_kreatifitas = self._get_nilai_task_kreatifitas(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            if nilai_skp and nilai_skp.get(skp_employee_obj.id)>0.0:
                nilai_total +=  nilai_skp.get(skp_employee_obj.id) 
            if nilai_perilaku and nilai_perilaku.get(skp_employee_obj.id)>0.0:
                nilai_total +=  nilai_perilaku.get(skp_employee_obj.id) 
            if nilai_tambahan and nilai_tambahan>0.0:
                nilai_total +=  nilai_tambahan
            if nilai_kreatifitas and nilai_kreatifitas>0.0:
                nilai_total +=  nilai_kreatifitas
            
            res[skp_employee_obj.id] =nilai_total
        return res
    def _get_nilai_tpp(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        rupiah=50
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            tpp=0.0
            
            nilai_tpp = self._get_nilai_total(cr, uid, [skp_employee_obj.id], field_names, args, context)
            
            if nilai_tpp and nilai_tpp.get(skp_employee_obj.id)>0.0:
                tpp =  nilai_tpp.get(skp_employee_obj.id) *rupiah
            res[skp_employee_obj.id] =round(tpp)
        return res
    def _get_fn_nilai_tambahan(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        nilai_tambahan=0
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            nilai_tambahan = self._get_nilai_task_tambahan(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            res[skp_employee_obj.id] =nilai_tambahan
        return res
    def _get_fn_nilai_kreatifitas(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        nilai_kreatifitas=0
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            nilai_kreatifitas = self._get_nilai_task_kreatifitas(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            res[skp_employee_obj.id] =nilai_kreatifitas
        return res
    def _get_jml_task_perilaku(self, cr, uid, user_id,target_period_month,target_period_year,state, context=None):
        
        task_pool = self.pool.get('project.perilaku')
        task_ids =task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_year','=', target_period_year),('target_period_month','=',target_period_month),('state','=',state)
                                             ], context=None)
        if task_ids:
            return len(task_ids)
        return 0;
    def _get_detail_nilai_skp(self, cr, uid, user_id,target_period_month,target_period_year, context=None):

        task_pool = self.pool.get('project.skp')
        task_ids=task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_year','=', target_period_year),('target_period_month','=',target_period_month),('state','=',('done'))
                                             ], context=None)
        task_list = task_pool.read(cr, uid, task_ids, ['nilai_akhir'], context=context)

        task_all_ids =task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_year','=', target_period_year),('target_period_month','=',target_period_month),('state','not in',('cancelled','draft'))
                                            ], context=None)

        jml_skp = 0
        jml_semua_skp = 0
        if task_all_ids:
            jml_skp= len(task_ids)
        if task_all_ids:
            jml_semua_skp= len(task_all_ids)

        nilai_akhir_skp=0
        nilai_total_skp=0
        for task_obj in task_list:
            nilai_akhir_skp=nilai_akhir_skp+task_obj['nilai_akhir']

        ret_skp_state_count = ''
        ret_jml_skp = jml_skp
        ret_jml_semua_skp = jml_semua_skp
        ret_nilai_skp=0
        ret_nilai_skp_percent=0
        if jml_skp == jml_semua_skp :
            ret_skp_state_count = 'Semua Kegiatan Selesai'
        else :
            ret_skp_state_count = str(jml_skp)+' / '+str(jml_semua_skp)+' Kegiatan SKP'

        if jml_semua_skp :
            nilai_total_skp = nilai_akhir_skp
            if jml_semua_skp > 0 and nilai_total_skp > 0 :
                nilai_skp = nilai_total_skp/jml_semua_skp;
            else :
                nilai_skp = 0
        else :
            nilai_skp = 0

        if nilai_skp>0.0:
                ret_nilai_skp_percent =  nilai_skp * 0.6
        else :
                ret_nilai_skp_percent =0.0

        ret_nilai_skp =nilai_skp

        return ret_skp_state_count,ret_jml_skp,ret_jml_semua_skp,ret_nilai_skp,ret_nilai_skp_percent;


    def _get_detail_query_skp(self, cr, uid, user_id,target_period_month,target_period_year, context=None):
        #HARUS DIBUAT INDEKS
        domain= target_period_year,target_period_month,user_id
        query = """ select state ,count(id) ,sum(coalesce(nilai_akhir,0)) from project_skp
                    where target_period_year=%s
                    and target_period_month =%s
                    and user_id = %s
                    and active
                    group by state

                                """
        cr.execute(query,domain)
        rs_sum_skp = cr.fetchall()
        jml_skp = 0
        jml_semua_skp=0
        nilai_akhir_skp=0

        for r_state,r_cnt,r_nilai in rs_sum_skp:
            jml_semua_skp += r_cnt
            if r_state == 'done':
               jml_skp += r_cnt
            nilai_akhir_skp +=r_nilai




        ret_skp_state_count = ''
        ret_jml_skp = jml_skp
        ret_jml_semua_skp = jml_semua_skp
        nilai_total_skp=0
        ret_nilai_skp=0
        ret_nilai_skp_percent=0
        if jml_skp == jml_semua_skp :
            ret_skp_state_count = 'Semua Kegiatan Selesai'
        else :
            ret_skp_state_count = str(jml_skp)+' / '+str(jml_semua_skp)+' Kegiatan SKP'

        if jml_semua_skp :
            nilai_total_skp = nilai_akhir_skp
            if jml_semua_skp > 0 and nilai_total_skp > 0 :
                nilai_skp = nilai_total_skp/jml_semua_skp;
            else :
                nilai_skp = 0
        else :
            nilai_skp = 0

        if nilai_skp>0.0:
                ret_nilai_skp_percent =  nilai_skp * 0.6
        else :
                ret_nilai_skp_percent =0.0

        ret_nilai_skp =nilai_skp

        return ret_skp_state_count,ret_jml_skp,ret_jml_semua_skp,ret_nilai_skp,ret_nilai_skp_percent;

    def _get_detail_nilai_tambahan(self, cr, uid, user_id,target_period_month,target_period_year,context=None):

        task_pool = self.pool.get('project.tambahan.kreatifitas')
        task_ids =task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_year','=', target_period_year),('target_period_month','=',target_period_month),('state','=','done'),
                                             ],
                                                limit=1,order='id desc',context=None)
        task_list = task_pool.read(cr, uid, task_ids, ['nilai_tambahan','nilai_kreatifitas'], context=context)

        nilai_tambahan=0
        nilai_kreatifitas=0
        for task_obj in task_list:
            nilai_tambahan=nilai_tambahan+task_obj['nilai_tambahan']
            nilai_kreatifitas=nilai_kreatifitas+task_obj['nilai_kreatifitas']

        return nilai_tambahan,nilai_kreatifitas

    def _get_detail_nilai_all_perilaku(self, cr, uid, user_id,target_period_month,target_period_year, context=None):

        task_pool = self.pool.get('project.perilaku')
        task_ids =task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_year','=', target_period_year),('target_period_month','=',target_period_month),('state','in',('done','closed'))
                                             ],
                                             limit=1,order='id desc',context=None)
        task_list = task_pool.read(cr, uid, task_ids, ['nilai_pelayanan','nilai_disiplin','nilai_komitmen','nilai_integritas','nilai_kerjasama','nilai_kepemimpinan','nilai_akhir'], context=context)
        jml_perilaku =len(task_ids)
        nilai_perilaku =0
        nilai_perilaku_percent=0.0
        for task_obj in task_list:
            nilai_total_perilaku =task_obj['nilai_akhir']
            if jml_perilaku > 0:
                nilai_perilaku = nilai_total_perilaku/jml_perilaku;
            if nilai_perilaku>0.0:
               nilai_perilaku_percent =  nilai_perilaku * 0.4

            return jml_perilaku,nilai_perilaku,nilai_perilaku_percent,task_obj['nilai_pelayanan'],task_obj['nilai_disiplin'],task_obj['nilai_komitmen'],task_obj['nilai_integritas'],task_obj['nilai_kerjasama'],task_obj['nilai_kepemimpinan'],

        return jml_perilaku,nilai_perilaku,nilai_perilaku_percent,0,0,0,0,0,0;

    def _get_detail_nilai_skp_employee(self, cr, uid, user_id,target_period_month,target_period_year, context=None):

        skp_state_count,jml_skp,jml_all_skp,nilai_skp,nilai_skp_percent=self._get_detail_nilai_skp(cr, uid, user_id,target_period_month,target_period_year, context=None)
        fn_nilai_tambahan,fn_nilai_kreatifitas=self._get_detail_nilai_tambahan(cr, uid, user_id,target_period_month,target_period_year, context=None)
        if nilai_skp:
            nilai_skp_tambahan_percent = ( nilai_skp * 0.6 )  + (fn_nilai_tambahan or 0) + (fn_nilai_kreatifitas or 0)
        else :
            nilai_skp_tambahan_percent =0.0
        jml_perilaku,nilai_perilaku,nilai_perilaku_percent,nilai_pelayanan,nilai_disiplin,nilai_komitmen,nilai_integritas,nilai_kerjasama,nilai_kepemimpinan = self._get_detail_nilai_all_perilaku(cr,uid,user_id,target_period_month,target_period_year,context)
        nilai_total = 0.0
        nilai_tpp=0
        if nilai_skp_tambahan_percent and nilai_skp_tambahan_percent>0.0:
            nilai_total +=  nilai_skp_tambahan_percent
        if nilai_perilaku_percent and nilai_perilaku_percent>0.0:
            nilai_total +=  nilai_perilaku_percent

        return skp_state_count,jml_skp,jml_all_skp,nilai_skp,nilai_skp_percent,fn_nilai_tambahan,fn_nilai_kreatifitas,nilai_skp_tambahan_percent,jml_perilaku,nilai_perilaku,nilai_perilaku_percent,nilai_pelayanan,nilai_disiplin,nilai_komitmen,nilai_integritas,nilai_kerjasama,nilai_kepemimpinan,nilai_total,nilai_tpp


    def _get_jml_task_skp(self, cr, uid, user_id,target_period_month,target_period_year,state, context=None):
        
        task_pool = self.pool.get('project.skp')
        task_ids =task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_year','=', target_period_year),('target_period_month','=',target_period_month),('state','=',state)
                                             ], context=None)
        if task_ids:
            return len(task_ids)
        return 0;
    def _get_jml_all_task_skp(self, cr, uid, user_id,target_period_month,target_period_year, context=None):
        
        task_pool = self.pool.get('project.skp')
        task_ids =task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_year','=', target_period_year),('target_period_month','=',target_period_month),('state','not in',('cancelled','draft'))
                                            ], context=None)
        if task_ids:
            return len(task_ids)
        return 0;
    def _get_nilai_task_tambahan(self, cr, uid, user_id,target_period_month,target_period_year,state, context=None):
        
        task_pool = self.pool.get('project.tambahan.kreatifitas')
        task_ids =task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_year','=', target_period_year),('target_period_month','=',target_period_month),('state','=',state),
                                             ],
                                                limit=1,order='id desc',context=None)
        task_list = task_pool.read(cr, uid, task_ids, ['nilai_tambahan'], context=context)
        nilai_total =0
        for task_obj in task_list:  
            nilai_total=nilai_total+task_obj['nilai_tambahan']
        return nilai_total;
    def _get_nilai_task_kreatifitas(self, cr, uid, user_id,target_period_month,target_period_year,state, context=None):
        
        task_pool = self.pool.get('project.tambahan.kreatifitas')
        task_ids =task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_year','=', target_period_year),('target_period_month','=',target_period_month),('state','=',state)
                               				], 
                                             limit=1,order='id desc',context=None)
        task_list = task_pool.read(cr, uid, task_ids, ['nilai_kreatifitas'], context=context)
        nilai_total =0
        for task_obj in task_list: 
            nilai_total=nilai_total+task_obj['nilai_kreatifitas']
        return nilai_total;
    def _get_nilai_task_perilaku(self, cr, uid, user_id,target_period_month,target_period_year,state, context=None):
        
        task_pool = self.pool.get('project.perilaku')
        task_ids =task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_year','=', target_period_year),('target_period_month','=',target_period_month),('state','=',state)
                                             ],
                                             limit=1,order='id desc',context=None)
        task_list = task_pool.read(cr, uid, task_ids, ['nilai_akhir'], context=context)
        nilai_total =0
        for task_obj in task_list: 
            nilai_total=nilai_total+task_obj['nilai_akhir']
        return nilai_total;
    def _get_nilai_task_skp(self, cr, uid, user_id,target_period_month,target_period_year,state, context=None):
        
        task_pool = self.pool.get('project.skp')
        task_ids=task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_year','=', target_period_year),('target_period_month','=',target_period_month),('state','=',state)
                                             ], context=None)
        task_list = task_pool.read(cr, uid, task_ids, ['nilai_akhir'], context=context)
        
        nilai_total =0
        for task_obj in task_list: 
            nilai_total=nilai_total+task_obj['nilai_akhir']
        return nilai_total; 
    def _get_jml_perilaku(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        jml_perilaku=0
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            jml_perilaku = self._get_jml_task_perilaku(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            
            res[skp_employee_obj.id] =jml_perilaku
        return res
    
    def _get_jml_skp(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        jml_skp=0
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            jml_skp = self._get_jml_task_skp(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            
            res[skp_employee_obj.id] =jml_skp
        return res
    def _get_jml_all_skp(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        jml_skp=0
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            jml_skp = self._get_jml_all_task_skp(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,context)
            
            res[skp_employee_obj.id] =jml_skp
        return res
    
    def _get_status_nilai_skp(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        status_nilai_skp='Masih Ada Kegiatan Yang Belum Dinilai'
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            jml_skp = self._get_jml_task_skp(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            jml_semua_skp = self._get_jml_all_task_skp(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year ,context)
           
            if jml_skp == jml_semua_skp :
                res[skp_employee_obj.id] = 'Semua Kegiatan Selesai'                
            else :
                res[skp_employee_obj.id] = str(jml_skp)+' / '+str(jml_semua_skp)+' Kegiatan SKP'
        return res
    def _get_nilai_skp(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        nilai_skp=0
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            jml_skp = self._get_jml_task_skp(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            jml_semua_skp = self._get_jml_all_task_skp(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year ,context)
            if jml_semua_skp :
                nilai_total_skp = self._get_nilai_task_skp(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
                if jml_semua_skp > 0 and nilai_total_skp > 0 :
                    nilai_skp = nilai_total_skp/jml_semua_skp;
                    res[skp_employee_obj.id] =nilai_skp
                else :
                    res[skp_employee_obj.id] = 0
            else :
                res[skp_employee_obj.id] = 0
        return res
    def _get_nilai_skp_sementara(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        nilai_skp=0
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            jml_skp = self._get_jml_task_skp(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            jml_semua_skp = jml_skp#self._get_jml_all_task_skp(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year ,context)
            if jml_skp == jml_semua_skp :
                nilai_total_skp = self._get_nilai_task_skp(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
                if jml_skp > 0 and nilai_total_skp > 0 :
                    nilai_skp = nilai_total_skp/jml_skp;
                    res[skp_employee_obj.id] =nilai_skp
                else :
                    res[skp_employee_obj.id] = 0
            else :
                res[skp_employee_obj.id] = 0
        return res
    def _get_nilai_perilaku(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        nilai_perilaku=0
        for skp_employee_obj in self.browse(cr, uid, ids, context=context):
            jml_perilaku = self._get_jml_task_perilaku(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            nilai_total_perilaku = self._get_nilai_task_perilaku(cr, uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,'done' ,context)
            if jml_perilaku > 0 and nilai_total_perilaku > 0 :
                nilai_perilaku = nilai_total_perilaku/jml_perilaku;
                res[skp_employee_obj.id] =nilai_perilaku
            else :
                res[skp_employee_obj.id] = 0
        return res 
    def get_detail_nilai_perilaku(self, cr, uid, user_id,target_period_month,target_period_year, context=None):
        
        task_pool = self.pool.get('project.perilaku')
        task_ids =task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_year','=', target_period_year),('target_period_month','=',target_period_month),('state','in',('done','closed'))
                                             ],
                                             limit=1,order='id desc',context=None)
        task_list = task_pool.read(cr, uid, task_ids, ['nilai_pelayanan','nilai_disiplin','nilai_komitmen','nilai_integritas','nilai_kerjasama','nilai_kepemimpinan'], context=context)
        nilai_total =0
        for task_obj in task_list: 
            return task_obj['nilai_pelayanan'],task_obj['nilai_disiplin'],task_obj['nilai_komitmen'],task_obj['nilai_integritas'],task_obj['nilai_kerjasama'],task_obj['nilai_kepemimpinan'],
        return 0,0,0,0,0,0;

    def insert_skp_employee_query(self, cr, employee_id,user_id,target_period_year,target_period_month,company_id,department_id,biro_id,is_kepala_opd):
        insert_query = """ INSERT INTO skp_employee(employee_id,user_id,target_period_year,target_period_month
                        ,company_id,department_id,biro_id,is_kepala_opd)
                        VALUES (%s,%s,%s,%s,%s,%s,%s,%s)

                                """
        cr.execute(insert_query, (employee_id,user_id,target_period_year,target_period_month,company_id,department_id,biro_id,is_kepala_opd))


        
    _columns = {
         'target_period_year'     : fields.char('Periode Tahun',size=4, required=True),
         'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')],'Periode Bulan'
                                                     ),
        'employee_id': fields.many2one('res.partner', 'Pegawai Yang Dinilai', readonly=True),
        'is_kepala_opd': fields.related('employee_id', 'is_kepala_opd',  type='boolean', string='Kepala OPD', store=True),
        'user_id': fields.many2one('res.users','User Login', readonly=True ),
        'company_id': fields.related('employee_id', 'company_id', relation='res.company', type='many2one', string='OPD', store=True),
        'department_id': fields.related('employee_id', 'department_id', relation='partner.employee.department', type='many2one', string='Bidang', store=True),
        'biro_id': fields.related('employee_id', 'biro_id', relation='partner.employee.biro', type='many2one', string='Biro', store=True),
        
        'skp_state_count': fields.function(_get_status_nilai_skp, string='Jml Kegiatan Yg Sudah Dinilai', type='char', help="Status Nilai SKP",
           method=True, store = True), #TODO Jika ada target yang belum diterima
        'jml_skp': fields.function(_get_jml_skp, string='Jumlah SKP', type='integer', help="Jumlah SKP",
            method=True, store = True),
        'jml_all_skp': fields.function(_get_jml_all_skp, string='Jumlah Semua SKP', type='integer', help="Jumlah SKP",
            method=True, store = True),
        'jml_perilaku': fields.function(_get_jml_perilaku, string='Jumlah Perilaku', type='integer', help="Jumlah Perilaku",
            method=True, store = True),
        'nilai_skp': fields.function(_get_nilai_skp, string='Nilai SKP', type='float', help="Nilai SKP Akan Muncul Apabila Semua Kegiatan Dibulan Tertentu Sudah Selesai Dinilai",
            method=True, store = True),
        'nilai_skp_percent': fields.function(_get_nilai_skp_percent, string='Nilai SKP(%)', type='float', help="60% Dari Kegiatan DPA Biro, APBN, SOTK",
            method=True, store = True),
        'nilai_skp_tambahan_percent': fields.function(_get_nilai_skp_tambahan_percent, string='Nilai SKP + Tambahan + Kreatifitas(%)', type='float', help="60% * SKP + Tambahan + Kreatifitas",
            method=True, store = True),        
        'nilai_perilaku': fields.function(_get_nilai_perilaku, string='Nilai Perilaku', type='float', help="40% Kontribusi untuk nilai perilaku",
            method=True, store = True),
        'nilai_perilaku_percent': fields.function(_get_nilai_perilaku_percent, string='Nilai Perilaku(%)', type='float', help="40% Kontribusi untuk nilai perilaku",
            method=True, store = True),
        'fn_nilai_tambahan': fields.function(_get_fn_nilai_tambahan, string='Nilai Tambahan', type='float', help="Nilai Tambahan",
            method=True, store = True),
        'fn_nilai_kreatifitas': fields.function(_get_fn_nilai_kreatifitas, string='Nilai Kreatifitas', type='float', help="Nilai Kreatifitas",
            method=True, store = True),
        'nilai_total': fields.function(_get_nilai_total, string='Nilai Total (%)', type='float', help="60% SKP + maks 2, Nilai Tambahan + 40% perilaku",
            method=True, store = True),
        'nilai_tpp': fields.function(_get_nilai_tpp, string='TPP', type='float', help="TPP",
            method=True, store = True),
                
        'nilai_kepemimpinan': fields.float('Nilai Kepemimpinan', readonly=True),
        'nilai_kerjasama': fields.float('Nilai Kerjasama', readonly=True),
        'nilai_integritas': fields.float('Nilai Integritas', readonly=True),
        'nilai_komitmen': fields.float('Nilai Komitmen', readonly=True),
        'nilai_disiplin': fields.float('Nilai Disiplin', readonly=True),
        'nilai_pelayanan': fields.float('Nilai Pelayanan', readonly=True),
        'nilai_akhir': fields.float( 'Nilai',readonly=True),
   }
    _sql_constraints = [
         ('data_unique', 'unique (employee_id,target_period_year,target_period_month)',
             'Rekap Tidak Bisa dibuat,Karena Terjadi Duplikasi')
     ]
skp_employee()




