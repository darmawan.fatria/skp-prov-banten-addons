#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime,timedelta
import time
from openerp.osv import osv
from openerp.report import report_sxw
import locale


class report_skp_form_yearly_report(report_sxw.rml_parse):
    
    def __init__(self, cr, uid, name, context):
        super(report_skp_form_yearly_report, self).__init__(cr, uid, name, context=context)
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        self.localcontext.update({
            'get_total': self.get_total,
            'get_atribut_kepegawaian':self.get_atribut_kepegawaian,
            'get_concat_nilai_skp' : self.get_concat_nilai_skp,
            'get_recap_yearly_report_raw' : self.get_recap_yearly_report_raw,
            'parsing_nilai_kepemimpinan':self.parsing_nilai_kepemimpinan,
            'get_rata_rata_nilai_perilaku':self.get_rata_rata_nilai_perilaku,
             'get_concat_nilai_perilaku':self.get_concat_nilai_perilaku,
            'get_concat_with_format_date':self.get_concat_with_format_date, 
            'get_format_date':self.get_format_date,
            'format_date_month' :self.format_date_month,
        })

        self.total = 0.0
        self.atribut_kepegawaian=None

    def get_atribut_kepegawaian(self,filters,context=None):
        period_year=filters['form']['period_year']
        user_id = False
        data_pegawai=None
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        user_pool = self.pool.get('res.users')
        if user_id:
            result = user_pool.browse(self.cr, self.uid, user_id)
            if  result and result.partner_id:
                data_pegawai = result.partner_id
        return data_pegawai;
    def get_recap_yearly_report_raw(self,filters,context=None):
        period_year=filters['form']['period_year']
        user_id = False
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        skp_yearly_pool = self.pool.get('skp.employee.yearly')
        skp_yearly_ids=skp_yearly_pool.search(self.cr, self.uid, [('target_period_year','=',period_year),('user_id','=',user_id)], context=None)
        results = skp_yearly_pool.browse(self.cr, self.uid, skp_yearly_ids)
        if results:
            return results[0];
        return None
    
    def get_concat_nilai_perilaku(self,nilai_perilaku,pengali,context=None):
        str_nilai_perilaku=''
        
        if nilai_perilaku:
            str_nilai_perilaku = nilai_perilaku
        ret_skp = "%s x %s" % (str_nilai_perilaku, pengali)
        return ret_skp
    def get_concat_nilai_skp(self,nilai_skp,pengali,tugas_tambahan,kreatifitas,context=None):
        str_nilai_skp='-'
        str_tugas_tambahan='0'
        str_kreatifitas='0'
        if nilai_skp:
            str_nilai_skp = nilai_skp
        if tugas_tambahan:
            str_tugas_tambahan = str(int(tugas_tambahan))
        if kreatifitas:
            str_kreatifitas = str(int(kreatifitas))
        #ret_skp = "(%s + %s + %s ) x %s " % (str_nilai_skp, str_tugas_tambahan,str_kreatifitas,pengali)
        ret_skp = "( "+str_nilai_skp+" + "+str_tugas_tambahan+" + "+str_kreatifitas+" ) x "+pengali+" "
        return ret_skp
    
    def parsing_nilai_kepemimpinan(self,skp_yearly_obj,type):
       val =None
       if skp_yearly_obj and skp_yearly_obj.employee_id and skp_yearly_obj.employee_id.job_type == 'struktural' :
            if type==1 :
                val = skp_yearly_obj.nilai_kepemimpinan
            if type==2 :
                val = skp_yearly_obj.indeks_nilai_kepemimpinan
       return val
    def get_rata_rata_nilai_perilaku(self,skp_yearly_obj):
       val =0
       if not skp_yearly_obj : return 0;
       if skp_yearly_obj and skp_yearly_obj.total_perilaku == 0 : return 0;
       
       if skp_yearly_obj and skp_yearly_obj.employee_id and skp_yearly_obj.employee_id.job_type == 'struktural' :
           val = skp_yearly_obj.total_perilaku/6
       else :
            val = skp_yearly_obj.total_perilaku/5
            
       return val
       
    def get_concat_with_format_date(self,str_prefix,filters,context=None):
        print_date=filters['form']['print_date'];
        try:
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(print_date,'%Y-%m-%d'))
        except:
            formatted_print_date=''
        return str_prefix+' '+formatted_print_date
    def get_format_date(self,print_date,addDays,context=None):
        dateFormat='%d %B %Y'
        try :
            timeNow = datetime.strptime(print_date,'%Y-%m-%d')
            if addDays!=0 :
                anotherTime = timeNow + timedelta(days=addDays)
            else :
                anotherTime =timeNow
            formatted_print_date = anotherTime.strftime(dateFormat)
        except :
            formatted_print_date=''
        return formatted_print_date
    def get_total(self):
        return 101
    def format_date_month(self,val,context=None):
        try :

            val_date =  datetime.strptime(val,'%Y-%m-%d')
            if val_date.strftime('%B') != 'Pebruari' :
                fmt_val = val_date.strftime('%d %B')
            else :
                fmt_val = val_date.strftime('%d') +' ' +'Februari';

            return fmt_val
        except:
            return None

   

class wrapped_report_skp_form_yearly(osv.AbstractModel):
    _name = 'report.df_skp_form_yearly_report.report_skp_form_yearly_report'
    _inherit = 'report.abstract_report'
    _template = 'df_skp_form_yearly_report.report_skp_form_yearly_report'
    _wrapped_report_class = report_skp_form_yearly_report


