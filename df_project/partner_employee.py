# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID
from datetime import date,datetime,timedelta
import time
from mx import DateTime
# ====================== res company ================================= #

class res_partner(osv.Model):
    _inherit = 'res.partner'
    #SKP
    #todo must be define on df_skp_employee
    def _last_year_nilai_skp(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('skp.employee.yearly')
        today = date.today()
        for id in ids:
            nilai_skp=0.0
            skp_yearly_ids=lookup_pool.search(cr,uid,[('employee_id','=',id),('target_period_year','=',(today.year-1))],order="target_period_year desc,id desc")
            if skp_yearly_ids:
                skp_yearly_obj=lookup_pool.browse(cr,uid,skp_yearly_ids[0])
                if  skp_yearly_obj:
                    nilai_skp=  skp_yearly_obj.nilai_total
            res[id]=nilai_skp
        return res
    def _last_year_tahun_skp(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('skp.employee.yearly')
        today = date.today()
        for id in ids:
            nilai_skp='2014'
            skp_yearly_ids=lookup_pool.search(cr,uid,[('employee_id','=',id),('target_period_year','=',(today.year-1))],order="target_period_year desc,id desc")
            if skp_yearly_ids:
                skp_yearly_obj=lookup_pool.browse(cr,uid,skp_yearly_ids[0])
                if  skp_yearly_obj:
                    nilai_skp=  skp_yearly_obj.target_period_year
            res[id]=nilai_skp
        return res
    def _current_nilai_skp(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('skp.employee.yearly')
        today = date.today()
        for id in ids:
            nilai_skp=0.0
            skp_yearly_ids=lookup_pool.search(cr,uid,[('employee_id','=',id),('target_period_year','=',today.year)],order="target_period_year desc,id desc")
            if skp_yearly_ids:
                skp_yearly_obj=lookup_pool.browse(cr,uid,skp_yearly_ids[0])
                if  skp_yearly_obj:
                    nilai_skp=  skp_yearly_obj.nilai_total
            res[id]=nilai_skp
        return res
    _columns = {
       #SKP
        'nilai_skp': fields.function(_current_nilai_skp, method=True,readonly=True , store=False,
                                         type="float", string='Nilai SKP'),
        'last_year_nilai_skp': fields.function(_last_year_nilai_skp, method=True,readonly=True , store=False,
                                         type="float", string='Nilai SKP Tahun Lalu'),
        'last_year_tahun_skp': fields.function(_current_nilai_skp, method=True,readonly=True , store=False,
                                        type="char", string='Tahun Terakhir SKP'),
        'skp_employee_yearly_ids': fields.one2many('skp.employee.yearly','employee_id', 'Nilai SKP Tahunan',readonly=True ), 
       
    }
    
    def update_target_and_realisasi(self, cr, uid, ids, context=None):
        skp_pool = self.pool.get('project.skp')
        perilaku_pool = self.pool.get('project.perilaku')
        tambahan_pool = self.pool.get('project.tambahan.kreatifitas')
        target_pool = self.pool.get('project.project')
        if not isinstance(ids, list): ids = [ids]
        for employee in self.browse(cr, uid, ids, context=context):
            vals = {}
            now=DateTime.today();
            task_ids = skp_pool.search(cr,uid,[('target_period_year','=',now.year),('user_id','=',employee.user_id.id),('company_id','=',employee.company_id.id)],context=None)
            task_perilaku_ids = perilaku_pool.search(cr,uid,[('target_period_year','=',now.year),('user_id','=',employee.user_id.id),('state','!=','done')],context=None)
            task_tambahan_ids = tambahan_pool.search(cr,uid,[('target_period_year','=',now.year),('user_id','=',employee.user_id.id),('state','!=','done')],context=None)
            target_ids = target_pool.search(cr,uid,[('target_period_year','=',now.year),('user_id','=',employee.user_id.id),('company_id','=',employee.company_id.id)],context=None)
            if not employee.user_id_atasan :
                    if not employee.user_id_atasan.user_id:
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Data Kepegawaian Belum Lengkap.'))
                    else :
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Data Kepegawaian Belum Lengkap.'))
            if not employee.user_id_banding:
                    if not employee.user_id_banding.user_id:
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Data Kepegawaian Belum Lengkap.'))
                    else :
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Data Kepegawaian Belum Lengkap.'))
            if not employee.company_id:
                    if not employee.company_id.user_id_bkd:
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Data Kepegawaian Belum Lengkap.'))
            
                
            update_pic = {
                                    'user_id_atasan': employee.user_id_atasan.user_id.id,
                                    'user_id_banding': employee.user_id_banding.user_id.id,
                                    'user_id_bkd': employee.company_id.user_id_bkd.id,
                                     }
            skp_pool.write(cr, SUPERUSER_ID, task_ids, update_pic)
            update_pic = {
                                    'user_id_atasan': employee.user_id_atasan.user_id.id,
                                    'user_id_banding': employee.user_id_banding.user_id.id,
                                    'user_id_bkd': employee.company_id.user_id_bkd.id,
                                    'company_id': employee.company_id.id,
                                     }
            perilaku_pool.write(cr, SUPERUSER_ID, task_perilaku_ids, update_pic)
            tambahan_pool.write(cr, SUPERUSER_ID, task_tambahan_ids, update_pic)
            if target_ids :
                for target_obj in target_pool.browse(cr, uid,target_ids ,  context=None):
                    if target_obj.user_id and target_obj.user_id.id!=employee.user_id.id :
                        continue;
                    update_pic = {
                                        'user_id_atasan': employee.user_id_atasan.user_id.id,
                                        'user_id_banding': employee.user_id_banding.user_id.id,
                                        'user_id_bkd': employee.company_id.user_id_bkd.id,
                                         }
                    target_pool.write(cr, SUPERUSER_ID, [target_obj.id], update_pic)
        
        return True   
    def reset_target_and_realisasi(self, cr, uid, ids, context=None):
        skp_pool = self.pool.get('project.skp')
        perilaku_pool = self.pool.get('project.perilaku')
        tambahan_pool = self.pool.get('project.tambahan.kreatifitas')
        target_pool = self.pool.get('project.project')
        if not isinstance(ids, list): ids = [ids]
        for employee in self.browse(cr, uid, ids, context=context):
            vals = {}
            now=DateTime.today();
            task_ids = skp_pool.search(cr,uid,[('target_period_year','=',now.year),('user_id','=',employee.user_id.id)],context=None)
            task_perilaku_ids = perilaku_pool.search(cr,uid,[('target_period_year','=',now.year),('user_id','=',employee.user_id.id),('state','!=','done')],context=None)
            task_tambahan_ids = tambahan_pool.search(cr,uid,[('target_period_year','=',now.year),('user_id','=',employee.user_id.id),('state','!=','done')],context=None)
            target_ids = target_pool.search(cr,uid,[('target_period_year','=',now.year),('user_id','=',employee.user_id.id)],context=None)
            if not employee.user_id_atasan :
                    if not employee.user_id_atasan.user_id:
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Data Kepegawaian Belum Lengkap.'))
                    else :
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Data Kepegawaian Belum Lengkap.'))
            if not employee.user_id_banding:
                    if not employee.user_id_banding.user_id:
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Data Kepegawaian Belum Lengkap.'))
                    else :
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Data Kepegawaian Belum Lengkap.'))
            if not employee.company_id:
                    if not employee.company_id.user_id_bkd:
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Data Kepegawaian Belum Lengkap.'))
            
                
            update_pic = {
                                    'user_id_atasan': employee.user_id_atasan.user_id.id,
                                    'user_id_banding': employee.user_id_banding.user_id.id,
                                    'user_id_bkd': employee.company_id.user_id_bkd.id,
                                    'company_id': employee.company_id.id,
                                     }
            skp_pool.write(cr, SUPERUSER_ID, task_ids, update_pic)
            update_pic = {
                                    'user_id_atasan': employee.user_id_atasan.user_id.id,
                                    'user_id_banding': employee.user_id_banding.user_id.id,
                                    'user_id_bkd': employee.company_id.user_id_bkd.id,
                                    'company_id': employee.company_id.id,
                                     }
            perilaku_pool.write(cr, SUPERUSER_ID, task_perilaku_ids, update_pic)
            tambahan_pool.write(cr, SUPERUSER_ID, task_tambahan_ids, update_pic)
            if target_ids :
                for target_obj in target_pool.browse(cr, uid,target_ids ,  context=None):
                    if target_obj.user_id and target_obj.user_id.id!=employee.user_id.id :
                        continue;
                    update_pic = {
                                        'user_id_atasan': employee.user_id_atasan.user_id.id,
                                        'user_id_banding': employee.user_id_banding.user_id.id,
                                        'user_id_bkd': employee.company_id.user_id_bkd.id,
                                        'company_id': employee.company_id.id,
                                         }
                    target_pool.write(cr, 1, [target_obj.id], update_pic)
        
        return True 
        
res_partner()


