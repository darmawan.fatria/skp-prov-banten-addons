# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import SUPERUSER_ID

# ====================== res company ================================= #

class res_company(osv.Model):
    _inherit = 'res.company'
    _columns = {
        'user_id_bkd': fields.many2one('res.users', 'Petugas Verifikatur BKD', 
            help='Staff dari Verifikatur SKP Dari BKD'),
        'employee_id_kepala_instansi': fields.many2one('res.partner', 'Kepala Instansi',  domain="[('employee','=',True)]",
            help='Kepala Badan, Dinas'),
        'code':fields.char('Kode',size=12)
       
    }
    def write(self, cr, uid, ids, vals, context=None):
        if not isinstance(ids, list): ids = [ids]
        
        if vals.get('employee_id_kepala_instansi',False):
            last_employee_id_kepala_instansi = None
            for company  in self.read(cr, uid, ids,['id','name','employee_id_kepala_instansi'], context=context):
                        if company['employee_id_kepala_instansi']:
                            last_employee_id_kepala_instansi = company['employee_id_kepala_instansi'][0]
        employee_pool = self.pool.get('res.partner')
        super(res_company, self).write(cr, uid, ids, vals, context=context)
        #update is kepala opd di partner
        if vals.get('employee_id_kepala_instansi',False):
            for company  in self.read(cr, uid, ids,['id','name','employee_id_kepala_instansi'], context=context):
                    if company['employee_id_kepala_instansi']:
                        new_kepala_instansi = company['employee_id_kepala_instansi'][0] 
                        employee_pool.write(cr, SUPERUSER_ID,new_kepala_instansi, {'is_kepala_opd':True}, context=context) 
                        employee_pool.write(cr, SUPERUSER_ID,last_employee_id_kepala_instansi, {'is_kepala_opd':False}, context=context)
        return True
res_company()


