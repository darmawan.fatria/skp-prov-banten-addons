from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
import openerp.addons.decimal_precision as dp

# ====================== Popup Class Object ================================= #
class project_perilaku_propose_rejected(osv.Model):
    _name = 'project.perilaku.propose.rejected'
    _description = 'Realisasi Bulanan Perilaku Ditolak'
    _columns = {
        'is_suggest': fields.boolean('Tambahkan Koreski Penilaian'),
        'notes'      : fields.text('Catatan Koreksi',required=True),
        'task_id'     : fields.many2one('project.perilaku', 'Realisasi', readonly=True),
        'jumlah_konsumen_pelayanan'     : fields.integer('Jumlah Pelayanan',required=True),
        'jumlah_tidakpuas_pelayanan'     : fields.integer('Jumlah Ketidakpuasan Pelayanan',required=True),
        'satuan_jumlah_konsumen_pelayanan': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen',required=True),
        'ketepatan_laporan_spj'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan SPJ', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_spj'),('active', '=', True)] ),
        'ketepatan_laporan_ukp4'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan UKP4', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_ukp4'),('active', '=', True)] ), 
        'efisiensi_biaya_operasional'     : fields.many2one('acuan.penilaian', 'Efisiensi Biaya Operasional Kantor', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'efisiensi_biaya_operasional'),('active', '=', True)] ),
        
        'integritas_presiden': fields.boolean('Presiden'),
        'integritas_gubernur': fields.boolean('Gubernur'),
        'integritas_kepalaopd': fields.boolean('Kepala OPD'),
        'integritas_atasan': fields.boolean('Atasan Langsung'),
        'integritas_lainlain': fields.boolean('Lain Lain'),
        'integritas_es1': fields.boolean('Pejabat Eselon I'),
        'integritas_es2': fields.boolean('Pejabat Eselon II'),
        'integritas_es3': fields.boolean('Pejabat Eselon III'),
        'integritas_es4': fields.boolean('Pejabat Eselon IV'),
        'integritas_hukuman': fields.selection([('ya', 'Terkena Hukuman Disiplin'), ('tidak', 'Tidak Terkena Hukuman Disiplin')],'Ada Hukuman Disiplin',help="Jika Ada Hukuman Disiplin, Atau Tidak " ,required=True),
        'integritas_hukuman_ringan': fields.boolean('Ringan'),
        'integritas_hukuman_sedang': fields.boolean('Sedang'),
        'integritas_hukuman_berat': fields.boolean('Berat'),
        'hadir_apel_pagi'     : fields.integer('Kehadiran Apel Pagi',required=True),
        'hadir_hari_kerja'     : fields.integer('Kehadiran Hari Kerja',required=True),
        'hadir_jam_kerja': fields.integer('Kehadiran Jam Kerja',required=True),
        'hadir_upacara_hari_besar': fields.integer('Kehadiran Upacara Hari Besar',required=True),
        'kerjasama_nasional': fields.boolean('Nasional'),
        'kerjasama_gubernur': fields.boolean('Provinsi'),
        'kerjasama_kepalaopd': fields.boolean('OPD'),
        'kerjasama_atasan': fields.boolean('Atasan Langsung'),
        'kerjasama_lainlain': fields.boolean('Lain Lain'),
        'kerjasama_rapat_atasan': fields.boolean('Rapat Atasan Langsung'),
        'kerjasama_rapat_perangkat_daerah': fields.boolean('Rapat Perangkat Daerah'),
        'kerjasama_rapat_provinsi': fields.boolean('Rapat Provinsi'),
        'kerjasama_rapat_nasional': fields.boolean('Rapat Nasional'),
        'kepemimpinan_nasional': fields.boolean('Nasional'),
        'kepemimpinan_gubernur': fields.boolean('Provinsi'),
        'kepemimpinan_kepalaopd': fields.boolean('OPD'),
        'kepemimpinan_atasan': fields.boolean('Atasan Langsung'),
        'kepemimpinan_lainlain': fields.boolean('Lain Lain'),
        'kepemimpinan_narsum_unitkerja': fields.boolean('Narasumber Kerja Atasan Langsung'),
        'kepemimpinan_narsum_perangkat_daerah': fields.boolean('Narasumber Perangkat Daerah'),
        'kepemimpinan_narsum_provinsi': fields.boolean('Narasumber Provinsi'),
        'kepemimpinan_narsum_nasional': fields.boolean('Narasumber Nasional'),
        'is_kepala_opd': fields.boolean('Kepala OPD'),
        'employee_job_type':fields.char('Jenis Jabatan',size=50)
       
    }
    def action_perilaku_propose_rejected(self, cr, uid, ids, context=None):
        """  Pengajuan Di tolak
        """
        vals = {}
        task_pool = self.pool.get('project.perilaku')
        task_obj = self.browse(cr, uid, ids[0], context=context)
        vals.update({
                                'suggest_jumlah_konsumen_pelayanan'       : task_obj.jumlah_konsumen_pelayanan,
                                'suggest_satuan_jumlah_konsumen_pelayanan': task_obj.satuan_jumlah_konsumen_pelayanan.id or None,
                                'suggest_jumlah_tidakpuas_pelayanan'      : task_obj.jumlah_tidakpuas_pelayanan,
                                'suggest_ketepatan_laporan_spj'           : task_obj.ketepatan_laporan_spj.id or None,
                                'suggest_ketepatan_laporan_ukp4'          : task_obj.ketepatan_laporan_ukp4.id or None,
                                'suggest_efisiensi_biaya_operasional'     : task_obj.efisiensi_biaya_operasional.id or None,
                                
                                'suggest_hadir_upacara_hari_besar': task_obj.hadir_upacara_hari_besar,
                                'suggest_hadir_hari_kerja'     : task_obj.hadir_hari_kerja,
                                'suggest_hadir_jam_kerja': task_obj.hadir_jam_kerja,
                                'suggest_hadir_apel_pagi': task_obj.hadir_apel_pagi,
                                
                                'suggest_integritas_presiden': task_obj.integritas_presiden,
                                'suggest_integritas_gubernur': task_obj.integritas_gubernur,
                                'suggest_integritas_kepalaopd': task_obj.integritas_kepalaopd,
                                'suggest_integritas_atasan': task_obj.integritas_atasan,
                                'suggest_integritas_es1': task_obj.integritas_es1,
                                'suggest_integritas_es2': task_obj.integritas_es2,
                                'suggest_integritas_es3': task_obj.integritas_es3,
                                'suggest_integritas_es4': task_obj.integritas_es4,
                                
                                'suggest_integritas_hukuman': task_obj.integritas_hukuman,
                                'suggest_integritas_hukuman_ringan': task_obj.integritas_hukuman_ringan,
                                'suggest_integritas_hukuman_sedang': task_obj.integritas_hukuman_sedang,
                                'suggest_integritas_hukuman_berat': task_obj.integritas_hukuman_berat,
                                
                                'suggest_kerjasama_nasional': task_obj.kerjasama_nasional,
                                'suggest_kerjasama_gubernur': task_obj.kerjasama_gubernur,
                                'suggest_kerjasama_kepalaopd': task_obj.kerjasama_kepalaopd,
                                'suggest_kerjasama_atasan':task_obj.kerjasama_atasan,
                                'suggest_kerjasama_rapat_nasional': task_obj.kerjasama_rapat_nasional,
                                'suggest_kerjasama_rapat_provinsi': task_obj.kerjasama_rapat_provinsi,
                                'suggest_kerjasama_rapat_perangkat_daerah': task_obj.kerjasama_rapat_perangkat_daerah,
                                'suggest_kerjasama_rapat_atasan': task_obj.kerjasama_rapat_atasan,
                                
                                'suggest_kepemimpinan_nasional': task_obj.kepemimpinan_nasional,
                                'suggest_kepemimpinan_gubernur': task_obj.kepemimpinan_gubernur,
                                'suggest_kepemimpinan_kepalaopd': task_obj.kepemimpinan_kepalaopd,
                                'suggest_kepemimpinan_atasan':  task_obj.kepemimpinan_atasan,
                                'suggest_kepemimpinan_narsum_nasional': task_obj.kepemimpinan_narsum_nasional,
                                'suggest_kepemimpinan_narsum_provinsi': task_obj.kepemimpinan_narsum_provinsi,
                                'suggest_kepemimpinan_narsum_perangkat_daerah': task_obj.kepemimpinan_narsum_perangkat_daerah,
                                'suggest_kepemimpinan_narsum_unitkerja': task_obj.kepemimpinan_narsum_unitkerja,
                                
                                'is_suggest' : True,
                                'notes_atasan' : task_obj.notes,
                                'state':'rejected_manager',
                                })               
                # end if           
        task_pool.write(cr, uid, [task_obj.task_id.id], vals, context)
        
    
project_perilaku_propose_rejected()
class project_perilaku_appeal_rejected(osv.Model):
    _name = 'project.perilaku.appeal.rejected'
    _description = 'Realisasi Bulanan Perilaku  Banding Ditolak'
    _columns = {
        'is_appeal': fields.boolean('Tambahkan Koreski Penilaian'),
        'notes'      : fields.text('Catatan Koreksi',required=True),
        'task_id'     : fields.many2one('project.perilaku', 'Realisasi', readonly=True),
        'jumlah_konsumen_pelayanan'     : fields.integer('Jumlah Pelayanan',required=True),
        'jumlah_tidakpuas_pelayanan'     : fields.integer('Jumlah Ketidakpuasan Pelayanan',required=True),
        'satuan_jumlah_konsumen_pelayanan': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen',required=True),
        'ketepatan_laporan_spj'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan SPJ', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_spj'),('active', '=', True)] ),
        'ketepatan_laporan_ukp4'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan UKP4', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_ukp4'),('active', '=', True)] ), 
        'efisiensi_biaya_operasional'     : fields.many2one('acuan.penilaian', 'Efisiensi Biaya Operasional Kantor', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'efisiensi_biaya_operasional'),('active', '=', True)] ),
        
        'integritas_presiden': fields.boolean('Presiden'),
        'integritas_gubernur': fields.boolean('Gubernur'),
        'integritas_kepalaopd': fields.boolean('Kepala OPD'),
        'integritas_atasan': fields.boolean('Atasan Langsung'),
        'integritas_lainlain': fields.boolean('Lain Lain'),
        'integritas_es1': fields.boolean('Pejabat Eselon I'),
        'integritas_es2': fields.boolean('Pejabat Eselon II'),
        'integritas_es3': fields.boolean('Pejabat Eselon III'),
        'integritas_es4': fields.boolean('Pejabat Eselon IV'),
        'integritas_hukuman': fields.selection([('ya', 'Terkena Hukuman Disiplin'), ('tidak', 'Tidak Terkena Hukuman Disiplin')],'Ada Hukuman Disiplin',help="Jika Ada Hukuman Disiplin, Atau Tidak " ,required=True),
        'integritas_hukuman_ringan': fields.boolean('Ringan'),
        'integritas_hukuman_sedang': fields.boolean('Sedang'),
        'integritas_hukuman_berat': fields.boolean('Berat'),
        'hadir_apel_pagi'     : fields.integer('Kehadiran Apel Pagi',required=True),
        'hadir_hari_kerja'     : fields.integer('Kehadiran Hari Kerja',required=True),
        'hadir_jam_kerja': fields.integer('Kehadiran Jam Kerja',required=True),
        'hadir_upacara_hari_besar': fields.integer('Kehadiran Upacara Hari Besar',required=True),
        'kerjasama_nasional': fields.boolean('Nasional'),
        'kerjasama_gubernur': fields.boolean('Provinsi'),
        'kerjasama_kepalaopd': fields.boolean('OPD'),
        'kerjasama_atasan': fields.boolean('Atasan Langsung'),
        'kerjasama_lainlain': fields.boolean('Lain Lain'),
        'kerjasama_rapat_atasan': fields.boolean('Rapat Atasan Langsung'),
        'kerjasama_rapat_perangkat_daerah': fields.boolean('Rapat Perangkat Daerah'),
        'kerjasama_rapat_provinsi': fields.boolean('Rapat Provinsi'),
        'kerjasama_rapat_nasional': fields.boolean('Rapat Nasional'),
        'kepemimpinan_nasional': fields.boolean('Nasional'),
        'kepemimpinan_gubernur': fields.boolean('Provinsi'),
        'kepemimpinan_kepalaopd': fields.boolean('OPD'),
        'kepemimpinan_atasan': fields.boolean('Atasan Langsung'),
        'kepemimpinan_lainlain': fields.boolean('Lain Lain'),
        'kepemimpinan_narsum_unitkerja': fields.boolean('Narasumber Kerja Atasan Langsung'),
        'kepemimpinan_narsum_perangkat_daerah': fields.boolean('Narasumber Perangkat Daerah'),
        'kepemimpinan_narsum_provinsi': fields.boolean('Narasumber Provinsi'),
        'kepemimpinan_narsum_nasional': fields.boolean('Narasumber Nasional'),
        'is_kepala_opd': fields.boolean('Kepala OPD'),
        'employee_job_type':fields.char('Jenis Jabatan',size=50)
       
    }
    def action_perilaku_appeal_rejected(self, cr, uid, ids, context=None):
        """  Pengajuan Banding Perilaku Di tolak
        """
        vals = {}
        task_pool = self.pool.get('project.perilaku')
        task_obj = self.browse(cr, uid, ids[0], context=context)
        vals.update({
                                'appeal_jumlah_konsumen_pelayanan'       : task_obj.jumlah_konsumen_pelayanan,
                                'appeal_satuan_jumlah_konsumen_pelayanan': task_obj.satuan_jumlah_konsumen_pelayanan.id or None,
                                'appeal_jumlah_tidakpuas_pelayanan'      : task_obj.jumlah_tidakpuas_pelayanan,
                                'appeal_ketepatan_laporan_spj'           : task_obj.ketepatan_laporan_spj.id or None,
                                'appeal_ketepatan_laporan_ukp4'          : task_obj.ketepatan_laporan_ukp4.id or None,
                                'appeal_efisiensi_biaya_operasional'     : task_obj.efisiensi_biaya_operasional.id or None,
                                
                                'appeal_hadir_upacara_hari_besar': task_obj.hadir_upacara_hari_besar,
                                'appeal_hadir_hari_kerja'     : task_obj.hadir_hari_kerja,
                                'appeal_hadir_jam_kerja': task_obj.hadir_jam_kerja,
                                'appeal_hadir_apel_pagi': task_obj.hadir_apel_pagi,
                                
                                'appeal_integritas_presiden': task_obj.integritas_presiden,
                                'appeal_integritas_gubernur': task_obj.integritas_gubernur,
                                'appeal_integritas_kepalaopd': task_obj.integritas_kepalaopd,
                                'appeal_integritas_atasan': task_obj.integritas_atasan,
                                'appeal_integritas_es1': task_obj.integritas_es1,
                                'appeal_integritas_es2': task_obj.integritas_es2,
                                'appeal_integritas_es3': task_obj.integritas_es3,
                                'appeal_integritas_es4': task_obj.integritas_es4,
                                
                                'appeal_integritas_hukuman': task_obj.integritas_hukuman,
                                'appeal_integritas_hukuman_ringan': task_obj.integritas_hukuman_ringan,
                                'appeal_integritas_hukuman_sedang': task_obj.integritas_hukuman_sedang,
                                'appeal_integritas_hukuman_berat': task_obj.integritas_hukuman_berat,
                                
                                'appeal_kerjasama_nasional': task_obj.kerjasama_nasional,
                                'appeal_kerjasama_gubernur': task_obj.kerjasama_gubernur,
                                'appeal_kerjasama_kepalaopd': task_obj.kerjasama_kepalaopd,
                                'appeal_kerjasama_atasan':task_obj.kerjasama_atasan,
                                'appeal_kerjasama_rapat_nasional': task_obj.kerjasama_rapat_nasional,
                                'appeal_kerjasama_rapat_provinsi': task_obj.kerjasama_rapat_provinsi,
                                'appeal_kerjasama_rapat_perangkat_daerah': task_obj.kerjasama_rapat_perangkat_daerah,
                                'appeal_kerjasama_rapat_atasan': task_obj.kerjasama_rapat_atasan,
                                
                                'appeal_kepemimpinan_nasional': task_obj.kepemimpinan_nasional,
                                'appeal_kepemimpinan_gubernur': task_obj.kepemimpinan_gubernur,
                                'appeal_kepemimpinan_kepalaopd': task_obj.kepemimpinan_kepalaopd,
                                'appeal_kepemimpinan_atasan':  task_obj.kepemimpinan_atasan,
                                'appeal_kepemimpinan_narsum_nasional': task_obj.kepemimpinan_narsum_nasional,
                                'appeal_kepemimpinan_narsum_provinsi': task_obj.kepemimpinan_narsum_provinsi,
                                'appeal_kepemimpinan_narsum_perangkat_daerah': task_obj.kepemimpinan_narsum_perangkat_daerah,
                                'appeal_kepemimpinan_narsum_unitkerja': task_obj.kepemimpinan_narsum_unitkerja,
                                
                                'is_appeal' : True,
                                'notes_atasan_banding' : task_obj.notes,
                                
                                })               
                # end if           
        
        task_pool.write(cr, uid, [task_obj.task_id.id], vals, context)
        task_pool.fill_task_automatically_with_appeal(cr, uid, [task_obj.task_id.id], context);
        task_pool.set_evaluated(cr, uid,  [task_obj.task_id.id], context=context)
        
    
project_perilaku_appeal_rejected()
class project_perilaku_verificate_rejected(osv.Model):
    _name = 'project.perilaku.verificate.rejected'
    _description = 'Realisasi Perilaku , Verifikasi Ditolak'
    _columns = {
        'is_control': fields.boolean('Tambahkan Koreski Verifikasi'),
        'notes'      : fields.text('Kesimpulan Catatan Koreksi',required=True),
        'control_count': fields.integer('Jumlah Koreksi Verifikasi', readonly=True),
        'task_id'     : fields.many2one('project.perilaku', 'Realisasi', readonly=True),
       
    }
    def action_verificate_rejected(self, cr, uid, ids, context=None):
        """  Pengajuan Di tolak
        """
        vals = {}
        task_pool = self.pool.get('project.perilaku')
        task_obj = self.browse(cr, uid, ids[0], context=context)
        vals.update({               'control_count'     : task_obj.control_count,
                                    'is_control' : True,
                                    'is_suggest' : True,
                                    'notes_bkd' : task_obj.notes,
                                    'state':'rejected_bkd',
                                     })
        task_pool.write(cr, uid, [task_obj.task_id.id], vals, context)
        
    
project_perilaku_verificate_rejected()