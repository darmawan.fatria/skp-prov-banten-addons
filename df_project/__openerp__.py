##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Target Dan Realisasi Untuk Penilaian Prestasi Kinerja",
    "version": "4.0",
    "author": "Darmawan Fatriananda",
    "category": "Kegiatan/Target Tahunan",
    "description": """
      Target Tahunan
    """,
    "website" : "http://www.mediasee.net",
    "license" : "GPL-3",
    "depends": [
                "mail",
                "df_partner_employee",
                "df_users"
                ],
    'data': [      
                   'security/project_security.xml',
                   'workflow/project_workflow.xml',
                   'company_view.xml',
                   
                   'project_view.xml',
                   'project_menu_view.xml',
                   'project_skp_view.xml',
                   'project_skp_menu_view.xml',
                   'popup_view/project_skp_popup_view.xml',
                   'project_tambahan_kreatifitas_view.xml',
                   'project_tambahan_kreatifitas_menu_view.xml',
                   'popup_view/project_tambahan_kreatifitas_popup_view.xml',
                   'project_perilaku_view.xml',
                   'project_perilaku_menu_view.xml',
                   'popup_view/project_perilaku_popup_view.xml',
                   'project_config_view.xml',
                   'project_partner_employee_view.xml',
                   
                   'partner_employee_view.xml',
                   ],
    
    'installable': True,
    'active': True,

}
